package mx.gob.fovissste;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 
 * @author Eder Sam
 */
public class HipotenusaXXX {

	private static Scanner SCANNER;

	public static void main(String[] args) {

		double legA = 0d;
		double legB = 0d;

		do {
			System.out.print("Introduce cateto a: ");
			legA = new HipotenusaXXX().getLeg();
		} while (legA <= 0);

		do {
			System.out.print("Introduce cateto b: ");
			legB = new HipotenusaXXX().getLeg();
		} while (legB <= 0);

		SCANNER.close();

		System.out.println("La hipotenusa es: " + Math.hypot(legA, legB));

	}

	private double getLeg() {

		double leg = 0d;

		try {
			SCANNER = new Scanner(System.in);
			leg = SCANNER.nextDouble();
		} catch (InputMismatchException ime) {
			System.out.println("Solo se aceptan números.");
		}

		if (leg <= 0) {
			System.out.println("El valor debe ser mayor a 0.");
		}

		return leg;
	}

}
